#!/usr/bin/perl

# Since: 2015/01/13
#
# Update: 2015/12/06
# Version: 1.1
#
# ※imapsync_jpとセットで動作します。

package IMAPSYNC::JPModule;

use strict;
use warnings;

use Encode qw/encode decode from_to/;
use Encode::IMAPUTF7;
use Encode::Guess qw/shift-jis euc-jp 7bit-jis/;

# 値照合の合理化の為、日本語名はIMAP-UTF-7で持たせる。
# key(Host1) => value(Host2) の形式で記載する。
# ※value値は、作成するフォルダ名にも使用される為、大小文字、誤字に注意。
my %transitional_names = (
        "受信トレイ" => "受信トレイ②",
        "Sent" => "Sent Items",
        "Trash" => "Deleted Items",
        "JunkMail" => "JunkEmail",
        #"Sent" => "送信済みアイテム",
        #"Trash" => "削除済みアイテム",
        "Drafts" => "下書き",
        #"JunkMail" => "迷惑メール",
        "TesInbox" => "受信トレイ②",
        "TestFolder" => "テストフォルダ"
        ## ↓サンプル
        #"Inbox" => "受信トレイ",
        #"Sent Items" => "送信済みアイテム",
        #"Deleted Items" => "削除済みアイテム",
        #"Notes" => "メモ",
        #"Junk Email" => "迷惑メール",
        #"送信済みアイテム" => "Sent Items",
        #"削除済みアイテム" => "Deleted Items",
        #"メモ" => "Notes",
        #"迷惑メール" => "Junk Email",
        #"下書き" => "Drafts",
        );

&convert_uppercase_and_modutf7;

# ↓サブルーチン

# キーの大文字化と値のIMAP-UTF7+UTF8化
# ※JPModule内でのみ利用。
sub convert_uppercase_and_modutf7 {
    foreach my $key (keys(%transitional_names)) {
        my $upper_key = uc(utf8jp_to_modutf7(decode('utf-8', $key)));
        my $value = utf8jp_to_modutf7(decode('utf-8', $transitional_names{$key}));
        
        delete $transitional_names{$key};
        $transitional_names{$upper_key} = $value;
    }
}
# IMAP修正UTF-7を、指定の文字コードへエンコードする。
# UTF-8以外での出力が必要な場合は、再考すること。
sub modutf7_to_utf8jp {
    my $string = shift;
    my $decode_lang = 'IMAP-UTF-7';
    my $encode_lang = 'utf-8';
    
    # 先頭且つルートフォルダの場合
    if ($string =~ m/^&.+?-$/) {
        return encode($encode_lang, decode($decode_lang, $string, 1));
        
    # 先頭以外の場合
    } elsif (my @matches = $string =~ m/(&.+?-)/g) {
        my $decoded_string;
        
        foreach my $matched_string (@matches) {
            $decoded_string = decode($decode_lang, $matched_string, 1);
            $string =~ s/\Q$matched_string\E/$decoded_string/;
        }
        return encode($encode_lang, $string);
        
    } else {
        return $string;
    }
}
# 指定日本語文字列からIMAP-UTF-7エンコードする。
# 文字コードがutf-8で無い場合は、処理前にutf-8でデコードする。
# ※JPModule内でのみ利用。
sub utf8jp_to_modutf7 {
    my $string = shift;
    my $lang = guess_lang($string);
    
    if ($lang eq 'utf8') {
        return encode('IMAP-UTF-7', $string, 1);
        
    } else {
        return $string;
    }
}
# MIME-Heaer(ISO-2022-JP/utf-8)からUTF-8へ変換する。
sub mimeheader_to_utf8 {
    my $string = shift;
    
    if ($string =~ m|\Q=?iso-2022-jp?B?\E.*|i) {
        return encode('utf-8', decode('MIME-Header', $string, 1), 1);
    } elsif ($string =~ m|\Q=?utf-8?B?\E.*|i) {
        return encode('utf-8', decode('MIME-Header', $string, 1), 1);
    } else {
        return $string;
    }
}
# 文字列の文字コードを判別する。※簡易
sub guess_lang {
    my $string = shift;
    my $encode = guess_encoding($string);
    
    ref($encode) || die "Cannot Encode::Guess $encode";
    
    return $encode->name;
}
# 別フォルダ名で移行したい場合の、フォルダ名変換を実行する。
# IMAP-UTF-7をベースとするため、出力時はmodutf7_to_utf8jpメソッドを通すこと。
sub convert_to_transitional_name {
    my ($string, $separator) = @_;
    my $key_string = uc($string);
    
    $key_string =~ /(.*?)\Q$separator\E.*?/is;
    
    # マッチしない場合は、大文字化した引数をそのまま使用する。
    my $matched_string = $1 || $key_string;
    
    if (exists($transitional_names{$matched_string})) {
        my $new_string = $transitional_names{$matched_string};
        $string =~ s/^$matched_string/$new_string/i;
    }
    return $string;
}

1;