#!/usr/bin/perl

# JPModule用 テストドライバ

package IMAPSYNC::JPModule;

use strict;
use warnings;

use Mail::IMAPClient;
use Encode qw/encode decode/;
use Encode::IMAPUTF7;
use Encode::Guess qw/sjis euc-jp 7bit-jis/;

require "./JPModule.pl";

my @folder_names = (
        'Inbox/&MMYwuTDIMNUwqTDrMMA-',
        'INBOX/&MA6KGF,1iowwD33ofoKVok,C-/&UxdnUQ-',
        'TesINBOX/&U1JpbXUf-',
        '&UWWKZg-',
        '&Uh0wrzDp-',
        'Junk Email/&UxdnUQ-',
        '&V,pullNUTxo-',
        '&VvNm+FnUVOFPGg-',
        '&ZVlS2Q-',
        '&cnl96A-',
        '&gepd8XC5aRyKVU+h-',
        '&mth7SWVZgLI-',
        'INBOX/12&MLkwrTDrMLo-',
        '12&MLww3w-',
        '12&aYKK1g-',
        '12independent study',
        '13&gvGKng-II',
        '14&V,p5DjC8MN8-',
        '15&MLww3w-',
        'FD&WdRU4U8a-',
        'FSHowell',
        'INBOX',
        'IR&WdRU4U8a-',
        '日本語',
        'English'
        );
        
#UTF-7デコードテスト
print "# Decode Test.\n";
print "# IMAP-UTF-7 to UTF-8+jp.\n\n";

foreach my $value (@folder_names) {
    my $decoded_string = modutf7_to_utf8jp($value);
    
    my $before_strings
        = decode('utf-8', $value) . " -> [" . $decoded_string . "]";
    
    my $after_temp = convert_to_transitional_name($value, '/');
    my $after_strings
        = $after_temp . " -> [" . modutf7_to_utf8jp($after_temp) . "]";
    
    print "\n  " . $before_strings . "\n";
    
    if ($before_strings ne $after_strings) {
        print "    -> Convert to transitional name.\n";
        print '  ' . $after_strings, "\n";
    }
}

#置換テスト
#print "\n# Convert Test.\n";

#foreach my $value (@folder_names) {
#    print ' ' . convert_to_transitional_name($value);
#    print " == ";
#    print modutf7_to_utf8jp(convert_to_transitional_name($value)), "\n";
#}


#UTF-7化テスト
print "\n# IMAP-UTF-7 Encode Test.\n";

my $encode_string = decode('utf-8', "てすと");
#print decode('utf-8', $encode_string) . ' -> ';
print encode('utf-8', $encode_string) . " -> ";

my $encoded_string = utf8jp_to_modutf7($encode_string);
print $encoded_string . " -> ";
 
my $decoded_string = modutf7_to_utf8jp($encoded_string);
print $decoded_string . "\n";


#文字コード判別テスト
print "\n# Lang Guess Test.\n";
print guess_lang($encode_string) . "\n";
print guess_lang($encoded_string) . "\n";

#ISO-2022-JPデコードテスト
print "\n# ISO-2022-JP Decode Test.\n";
print iso2022jp_to_utf8('=?iso-2022-jp?B?VGVzSW5ib3gbJEIhISIqISE8dT8uJUglbCUkISElMyVUITwlRhsoQg==?=') . "\n"; #受信トレイ　コピーテ
print iso2022jp_to_utf8('JIS変換テスト') . "\n"; #JIS変換テスト
print iso2022jp_to_utf8('=?iso-2022-jp?B?VGVzSW5ib3gbJEIhISE8dT8uJUglbCUkISElMyVUITwlRhsoQg==?=') . "\n"; #Xコード破壊による変換不可

print "fin.\n";